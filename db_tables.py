import sys
from enum import IntEnum
import hashlib
from sqlalchemy import Column, String, Integer
from sqlalchemy import Enum as EnumColumn
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import as_declarative
from mautrix.bridge.db import Base

class UserType(IntEnum):
    real = 1
    mailpuppet = 2

class PortalType(IntEnum):
    plumbed = 1
    dynamic = 2

class Users(Base):
    __tablename__ = 'users'
    # mail binded to matrix user (real or virtual)
    mxid = Column(String(255), primary_key=True, nullable=False)
    mail = Column(String(), nullable=False)
    usertype = Column(EnumColumn(UserType, nullable=False))

    @classmethod
    def get_mxid(cls, mail):
        result = cls.__table__.select().where(cls.mail == mail).execute().fetchone()
        if result == None:
            return None
        else:
            return result.mxid # TODO more than one result to handle aliases?

    @classmethod
    def get_mail(cls, mxid):
        result = cls.__table__.select().where(cls.mxid == mxid).execute().fetchone() # TODO more than one result to handle multiple addresses?
        if result == None:
            return None
        else:
            return result.mail

    @classmethod
    def queryByMxid(cls, mxid):
        return cls.__table__.select().where(cls.mxid == mxid).execute().fetchone() # TODO more than one result to handle multiple addresses?

    @classmethod
    def queryByMail(cls, mail):
        return cls.__table__.select().where(cls.mail == mail).execute().fetchone()

    @classmethod
    def insert(cls, mail, mxid, usertype):
        cls.__table__.insert().values(mail = mail, mxid = mxid, usertype = UserType(usertype)).execute()


class Rooms(Base):
    __tablename__ = 'rooms'
    # sha256 of null-separated list of mails or room email addr
    addr = Column(String(), primary_key=True, nullable=False)
    room = Column(String(), nullable=False)
    roomtype = Column(EnumColumn(PortalType, nullable=False))

    @classmethod
    def queryByAddr(cls, addr, roomtype):
        result = cls().__table__.select().where(cls.addr == addr).where(cls.roomtype == roomtype).execute().fetchone()
        if result == None:
            return None
        else:
            return result.room

    @classmethod
    def queryByRoomId(cls, room_id, roomtype):
        result = cls().__table__.select().where(cls.room == room_id).where(cls.roomtype == roomtype).execute().fetchone()
        if result == None:
            return None
        else:
            return result.addr

    @classmethod
    def insert(cls, roomId, addr, roomtype):
        cls.__table__.insert().values(room = roomId, addr = addr, roomtype = roomtype).execute()

    @classmethod
    def have(cls, roomId, roomtype):
        if cls.__table__.select().where(cls.room == roomId).where(cls.roomtype == roomtype).execute().fetchone() == None:
            return False
        else:
            return True
