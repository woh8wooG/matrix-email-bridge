#!/usr/bin/python3.7

import itertools
from enum import Enum
import copy
import hashlib
import asyncio, socket
import concurrent
import sqlalchemy
import email
import re
from abc import ABC
from io import StringIO
from email import policy
from email.generator import Generator
from smtplib import SMTP
from email.message import EmailMessage
from email.header import Header
from email.utils import formataddr
from aiosmtpd.controller import Controller
from aiosmtpd.lmtp import LMTP
from aiosmtpd.handlers import Message
from aiohttp import web
from mautrix.appservice import AppService
from mautrix.errors import MatrixError
from mautrix.bridge import Bridge, BaseMatrixHandler, BaseBridgeConfig, BaseUser, BasePortal, BasePuppet
from mautrix.bridge.commands import command_handler
from mautrix.client import Client
from mautrix.client.api.modules import MediaRepositoryMethods
from mautrix.appservice.api import IntentAPI
from mautrix.errors.request import MNotFound
from mautrix.types import UserID, RoomID, MessageEventContent, TextMessageEventContent, MediaMessageEventContent, EventID, Format, EventType
from typing import Dict, Any
import mautrix.bridge.db as bdb
import db_tables

MAIL_RE = re.compile(r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~\-]+@[a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*$')


class MatrixLmtpBridgeError(MatrixError, ABC):
    pass

# TODO matrix-side error reporting!
class TeporaryBridgeError(MatrixError):
    def __init__(self, reply='451 Requested action aborted: local error in processing'):
        self.reply = reply

class PernamentBridgeError(MatrixError):
    def __init__(self, reply='554 Transaction failed'):
        self.reply = reply


class Config(BaseBridgeConfig):
    @property
    def namespaces(self):
        return {
            'users': self['appservice.namespace.users'],
            'aliases': self['appservice.namespace.aliases']
        }


class LmtpHandler(Message):
    async def handle_DATA(self, server, session, envelope):
        env = copy.deepcopy(envelope)
        try:
            await LmtpPortal.handle_transaction(env)
        except MatrixLmtpBridgeError as e:
            return e.reply
        return '250 Message accepted for delivery' # TODO lmtp


def factory():
    return LMTP(LmtpHandler())


class LmtpUser(BaseUser):
    is_whitelisted: bool
    mxid: UserID
    mail: str
    usertype: db_tables.UserType
    command_status: Dict[str, Any]

    def __init__(self, mxid, mail, usertype):
        self.mail = mail
        self.mxid = mxid
        self.usertype = usertype

    @property
    def is_whitelisted(self):
        return LmtpBridge.test_perms(self.mxid, 'user')

    @classmethod
    def get_by_mail(cls, mail) -> 'LmtpUser':
        result = db_tables.Users.queryByMail(mail)

        if result and result.usertype == db_tables.UserType.real:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype))
        else:
            return None

    @classmethod
    def get_by_mxid(cls, mxid: UserID) -> 'LmtpUser':
        result = db_tables.Users.queryByMxid(mxid)
        if result and result.usertype == db_tables.UserType.real:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype))
        else:
            return cls(mxid, None, None)

    async def is_logged_in(self) -> bool:
        return True

    @classmethod
    def userMxid2email(cls, mxid):
        localPart = mxid.split(':')[0][1:]
        hs_domain = mxid.split(':')[1]
        mailLocalPart = \
            cls.config["bridge.user_mail_template"].format(localpart=localPart, \
                                                           hs_domain=hs_domain)
        domain = cls.config["bridge.user_domain"]
        return f"{mailLocalPart}@{domain}"


class LmtpPuppet(BasePuppet):
    is_whitelisted: bool
    mxid: UserID
    mail: str
    usertype: db_tables.UserType
    command_status: Dict[str, Any]

    def __init__(self, mxid, mail, usertype, is_whitelisted):
        self.mail = mail
        self.default_mxid = mxid
        self.usertype = usertype
        self.is_whitelisted = is_whitelisted

        self.custom_mxid = None

    @classmethod
    def get_by_mail(cls, mail) -> 'LmtpPuppet':
        result = db_tables.Users.queryByMail(mail)
        if not result:
            mxid = cls.insertUser(mail) # TODO do not create puppets in domain dedicated for users/rooms
        elif not result.usertype == db_tables.UserType.mailpuppet:
            return None
        else:
            mxid = result.mxid
        return cls(mxid, mail, db_tables.UserType.mailpuppet, True)

    @classmethod
    def get_by_mxid(cls, mxid: UserID) -> 'LmtpPuppet':
        result = db_tables.Users.queryByMxid(mxid)
        if result and result.usertype == db_tables.UserType.mailpuppet:
            return cls(result.mxid, result.mail,
                       db_tables.UserType(result.usertype), True)
        else:
            return None

    def save(self) -> None:
        pass

    @classmethod
    def insertUser(cls, mail):
        mxid = cls._mail2mxid(mail)
        db_tables.Users.insert(mail, mxid, db_tables.UserType.mailpuppet)
        asyncio.ensure_future(cls.az.intent.user(mxid).set_displayname(mail))
        return mxid

    ## It would be faster and easier to just use email address
    #  as mxid but it would not be possible for some email addresses.
    #  see https://stackoverflow.com/q/2049502
    #  and https://matrix.org/docs/spec/appendices#user-identifiers
    @classmethod
    def _mail2mxid(cls, mail):
        raw = mail.encode()
        hexdig = hashlib.sha256(raw).hexdigest()
        localPart = cls.config["bridge.username_template"].format(userid=hexdig)
        domain = cls.config["homeserver.domain"]
        return f"@{localPart}:{domain}"

    async def is_logged_in(self) -> bool:
        return True

    
class LmtpPortal(BasePortal):
    def __init__(self, room_id):
        self.room_id = room_id
        if db_tables.Rooms.have(room_id, db_tables.PortalType.dynamic):
            self._type = db_tables.PortalType.dynamic
        elif db_tables.Rooms.have(room_id, db_tables.PortalType.plumbed):
            self._type = db_tables.PortalType.plumbed
        else:
            return None

    @classmethod
    async def handle_transaction(cls, envelope):
        cls.log.debug(f'Handling email from {envelope.mail_from} to {envelope.rcpt_tos}')
        user_tos = []
        for addr in envelope.rcpt_tos:
            plumbed_id = db_tables.Rooms.queryByAddr(addr, db_tables.PortalType.plumbed)
            if plumbed_id:
                portal = LmtpPortal(plumbed_id)
                await portal.handle_email_message(envelope)
            else:
                user_tos.append(addr)
        if user_tos:
            user_tos.append(envelope.mail_from)
            portal = await LmtpPortal.get_by_mail_members(user_tos)
            await portal.handle_email_message(envelope)

    @classmethod
    async def get_by_mail_members(cls, membersAddr):
        addrhash = cls._rcpt2Hash(membersAddr)
        room_id = db_tables.Rooms.queryByAddr(addrhash, db_tables.PortalType.dynamic)

        if room_id == None: # TODO move it to separated method
            all_users = []
            puppets = []
            for memberAddr in membersAddr:
                usr = LmtpUser.get_by_mail(memberAddr)
                if usr:
                    try:
                        all_users.append(usr.mxid)
                    except AttributeError:
                        cls.log.warning(f"Failed to get user for {memberAddr}")
                else:
                    puppet = LmtpPuppet.get_by_mail(memberAddr)
                    if puppet.mxid:
                        puppets.append(puppet.mxid)
                        all_users.append(puppet.mxid)
                    else:
                        cls.log.warning(f"Failed to get puppet for {memberAddr}")

            if all_users:
                room_id = \
                    await cls.az.intent.create_room(name=' '.join(membersAddr),
                                                          invitees=all_users)
                db_tables.Rooms.insert(room_id, addrhash, db_tables.PortalType.dynamic)
                for puppet_id in puppets:
                    await cls.az.intent.user(puppet_id).join_room(room_id)
            else:
                cls.log.warning(f"Not creating empty room for {membersAddr}")
                raise PernamentBridgeError('554 no valid recipients')
        return cls(room_id)

    async def handle_email_message(self, envelope) -> None:
        ## Remember: If you are going to use envelope.rcpt_tos here,
        #  read method handle_transaction first
        # TODO invite all users again
        domain = self.az.domain
        sender = LmtpPuppet.get_by_mail(envelope.mail_from)
        msg = email.message_from_bytes(envelope.content, policy=policy.default)

        mimehtmlbody = msg.get_body(preferencelist=('html'))
        mimeplainbody = msg.get_body(preferencelist=('plain'))

        plainbody = htmlbody = None

        if mimeplainbody:
            plainbody = mimeplainbody.get_content()
        if mimehtmlbody:
            htmlbody = mimehtmlbody.get_content()

        await self.az.intent.user(sender.mxid).send_text(self.room_id, text=plainbody, html=htmlbody)

        for attachment in msg.iter_attachments():
            atname = attachment.get_filename()
            atmime = attachment.get_content_type()
            atbytes = attachment.get_content()

            aturl = await self.az.intent.user(sender.mxid).upload_media(data=atbytes, mime_type=atmime, filename=atname)
            await self.az.intent.user(sender.mxid).send_file(self.room_id, url=aturl, file_name=atname)

    async def handle_matrix_message(self, sender: 'BaseUser',
                                    message: MessageEventContent,
                                    event_id: EventID) -> None:
        puppets = await self.get_puppets()

        if not hasattr(self, '_type') or self._type == db_tables.PortalType.dynamic and \
               sender.usertype == db_tables.UserType.mailpuppet:
            self.log.warning(f"Dont know how to handle message with id {event_id} in room {self.room_id}")
            return

        # TODO 
        sender_or_puppet = LmtpPuppet.get_by_mxid(sender.mxid) or sender

        puppet_mail_addrs = []
        for puppet in puppets:
            assert puppet.mail
            if puppet.mxid == sender_or_puppet.mxid:
                continue
            puppet_mail_addrs.append(puppet.mail)

        if self._type == db_tables.PortalType.dynamic:
            if not sender_or_puppet.mail:
                self.log.warning(f"Sender {sender_or_puppet.mxid} have no mail assigned. Ignoring message {event_id}")
                return # It should not be possible...
            header_from = return_path = sender_or_puppet.mail
        else: # plumbed
            return_path = db_tables.Rooms.queryByRoomId(self.room_id, \
                                                        db_tables.PortalType.plumbed)
            display_name = await self.az.intent.get_displayname(sender_or_puppet.mxid)
            header_from = formataddr((str(Header(display_name, 'utf-8')), return_path))

        if not puppet_mail_addrs:
            self.log.warning(f"There is no valid mail puppet in room {self.room_id}")
            return

        mail_msg = EmailMessage()

        mail_msg['Subject'] = 'Bridged Matrix message' # TODO allow setting Subject with specially formatted msg
        mail_msg['To'] = puppet_mail_addrs
        mail_msg['From'] = header_from

        if type(message) == TextMessageEventContent:
            mail_msg.set_content(message.body)
            if message.formatted_body:
                if message.format == Format.HTML:
                    mail_msg.add_alternative(message.formatted_body, subtype='html')
                else:
                    self.log.warning(f"Message with id {event_id} in room {self.room_id} has unknown format.")
        elif type(message) == MediaMessageEventContent:
            try:
                maintype, subtype = message.info.mimetype.split('/', 1)
            except AttributeError:
                maintype = 'application'
                subtype = 'octet-stream'
            data = await self.az.intent.user(sender_or_puppet.mxid).download_media(message.url)
            mail_msg.add_attachment(data, maintype=maintype, subtype=subtype, filename=message.body)
        else:
            self.log.warning(f"Message with id {event_id} in room {self.room_id} has unknown type.")
            return

        with concurrent.futures.ThreadPoolExecutor() as executor:
            loop = asyncio.get_event_loop()
            await loop.run_in_executor(executor, self._sendmail, return_path, mail_msg)

    def _sendmail(self, return_path, msg):
        with SMTP(self.config["mta.hostname"], self.config["mta.port"], \
                  self.config["appservice.id"]) as smtp:
            smtp.sendmail(return_path, msg['To'], msg.as_bytes())

    async def get_puppets(self):
        memberIds = await self.az.intent.get_room_members(self.room_id)
        puppets = []
        for mxid in memberIds:
            puppet = LmtpPuppet.get_by_mxid(mxid)
            if puppet:
                puppets.append(puppet)
        return puppets

    @staticmethod
    def _rcpt2Hash(rcptList):
        sortedAddr = sorted(set(rcptList))
        raw = '\0'.join(sortedAddr).encode()
        return hashlib.sha256(raw).hexdigest()

    @classmethod
    def alias2mail(cls, alias):
        localpart = alias.split(':')[0][1:]
        if not re.match(r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~\-]', alias) or \
               re.match(r'.*\.\..*', alias):
            raise ValueError

        domain = cls.config["bridge.room_domain"]
        hs_domain = alias.split(':')[1]
        mail_localpart = \
            cls.config["bridge.room_mail_template"].format(localpart=localpart, \
                                                           hs_domain=hs_domain)
        return f'{mail_localpart}@{domain}'


class MatrixHandler(BaseMatrixHandler):
    async def get_user(self, user_id: UserID) -> 'BaseUser':
        return LmtpUser.get_by_mxid(user_id)

    async def get_portal(self, room_id: RoomID) -> 'BasePortal':
        return LmtpPortal(room_id)

    async def get_puppet(self, user_id: UserID) -> 'BasePuppet':
        return LmtpPuppet.get_by_mxid(user_id)


@command_handler()
async def msg(evt):
    mails = copy.deepcopy(evt.args)
    for mail in mails:
        if not MAIL_RE.match(mail):
            await evt.reply(f'ERROR: Mail "{mail}" is not valid.')
            return
    if not hasattr(evt.sender, 'mail') or type(evt.sender.mail) != str:
        await evt.reply(f'ERROR: Your matrix account have no email address.')
        return
    mails.append(evt.sender.mail)
    try:
        await LmtpPortal.get_by_mail_members(mails)
    except MatrixLmtpBridgeError as e:
        await evt.reply(f'ERROR: Failed to create room.')
        return

@command_handler()
async def register(evt):
    if evt.args:
        await evt.reply(f'This command does not take any arguments.')
        return

    mxid = evt.sender.mxid
    exiting_mail = db_tables.Users.get_mail(mxid)

    if exiting_mail:
        await evt.reply(f'You already have email: {exiting_mail}')
        return

    mail = LmtpUser.userMxid2email(mxid)
    db_tables.Users.insert(mail, mxid, db_tables.UserType.real)
    await evt.reply(f'Your bridged email address is: {mail}')

@command_handler()
async def bridge(evt):
    if evt.args:
        await evt.reply('This command does not take any arguments.')
        return

    lvls = await evt.az.intent.get_power_levels(evt.room_id, ignore_cache=True) # TODO cache is broken
    if not lvls.ensure_user_level(evt.sender.mxid, \
                                          evt.config['bridge.room_bridge_power_lvl']):
        await evt.reply('ERROR: You have too small power level to bridge this room.')
        return

    async def missing_alias():
        await evt.reply('ERROR: This room does not have canonical alias (main address).')

    try:
        alias_event = await evt.az.intent.get_state_event(evt.room_id, \
                                                          EventType.ROOM_CANONICAL_ALIAS)
    except MNotFound:
        await missing_alias()
        return
    if not alias_event or not hasattr(alias_event, 'canonical_alias') or \
       not alias_event.canonical_alias:
        await missing_alias()
        return

    try:
        addr = LmtpPortal.alias2mail(alias_event.canonical_alias)
    except ValueError:
        await evt.reply('ERROR: This room have invalid canonical alias (main address).')
        return

    existing_addr = db_tables.Rooms.queryByRoomId(evt.room_id, \
                                                  db_tables.PortalType.plumbed)
    if existing_addr:
        await evt.reply(f'This room already have email address: {exiting_addr}')
        return

    db_tables.Rooms.insert(evt.room_id, addr, db_tables.PortalType.plumbed)
    await evt.reply(f'Bridged email address for this room is: {addr}')


class LmtpBridge(Bridge):
    name='EmailBridge'
    description='test'
    command='placeholder'
    config_class=Config
    version='1.0'
    matrix_class=MatrixHandler

    @classmethod
    def test_perms(cls, mxid, role):
        perms = cls.config['bridge.permissions']
        for key in perms:
            if key == '*' and perms[key] == role:
                return True
            if key == mxid and perms[key] == role:
                return True
            user_domain = mxid.split(':')[1]
            if key == user_domain and perms[key] == role:
                return True

            return False

    def init_tables(self):
        for table in bdb.UserProfile, bdb.RoomState, db_tables.Users, db_tables.Rooms:
            table.db = self.db
            table.t = table.__table__
            table.c = table.t.c
        bdb.Base.metadata.create_all(self.db)

    def prepare_db(self) -> None:
        super().prepare_db()
        self.init_tables()


    def prepare_bridge(self):
        super().prepare_bridge()
        LmtpPortal.az = self.az
        LmtpPuppet.az = self.az # TODO?
        LmtpPortal.log = self.log
        LmtpPortal.config = LmtpPuppet.config = LmtpUser.config = LmtpBridge.config = self.config
        self.server = self.az.loop.run_until_complete(
            self.az.loop.create_server(
                factory, host=self.config["lmtp.bind"], port=self.config["lmtp.port"]))


def main():
    bridge = LmtpBridge()

    bridge.run()

main()
