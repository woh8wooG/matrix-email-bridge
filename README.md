# Matrix-email Bridge

This repo contains (almost) transparent matrix gateway.
Following features are implemented:
*  Sending and receiving emails, both plaintext and HTML.
   Bridge will dynamically create rooms and invite matrix users/mail puppets when new message appears, see 'Usage' section.
*  Turning matrix room into *something like* mailing list.
*  Sending and receiving attachments (looks ugly, see end of file).

### This project is currently in Beta state (and uses libs that are also in Beta to make life funny), some important features are still missing, see end of this file.

## Usage

### Dynamic (user) bridging

Invite bot and send command `!mb register`, bridge will generate email address for you.
From this moment people can send you emails and you will receive matrix messages.
If you want to start conversation yourself send command `!mb msg number1@email.com number2@another.email.net`.

Keep in mind that bridge assigns room to the set of message receivers,
so if email user anon@random.de will send a message to your bridged email new room will be created to handle messages between you and anon@random.de,
if anon@random.de will send a message to you and another@dude.jp new room will be created for messages that are sent between you, another@dude.jp and anon@random.de.

### Plumbed rooms

You can also use this software to bridge rooms. Just invite bot to room and send command `!mb bridge`.
Bridge will generate email address for room, when someone will send message to it, mail puppet will join room and send message.
When someone (including mail puppets) sends message in bridged room puppets that participate in room will get emails;
Bridge will set room's mail address in reply-path and 'From' header but with sender's matrix display name in email display name.

Remember that (by default) you need to have power lvl 90 to bridge room and room needs to have canonical alias (main address).
Additionally, at this moment, you need to allow anyone to join room, because otherwise mail puppets will not be able to join.

## Installation

As usual, make backups and do not do dangerous changes when server is under heavy load.
If you have bad luck, synapse/MTA may stop work, so be ready to undo changes at any moment.

### Mail server configuration

This bridge uses LMTP protocol and heavily depends on a 'real' SMTP server to handle message queuing, TLS, spam filtering and many other things.
In addition to the bridge configuration itself, you need to prepare an email server.

To avoid name collision between 'normal' email users and matrix users you need to get at least one additional MX record.
You also should add another one to avoid collision between rooms and users but you can tell bridge to use localpart prefixes,
so instead addresses like user1@matrix.domain.my and room1@room.domain.my you will get u_user1@bridge.domain.my and r_room1@bridge.domain.my,
see key 'user_mail_template' in example config.

Once DNS is configured you need to tell your mail server about your matrix gateway and redirect selected domains to it.

On exim you need to add router

```
matrix_gateway_router:
    driver = accept
    transport = matrix_gateway
    domains = matrix.domain.my:room.domain.my
```

and transport

```
matrix_gateway:
    driver = smtp
    protocol = lmtp
    hosts = localhost
    allow_localhost = true
    port = 8025
```

### Bridge configuration

Default configuration file and systemD service assume that you have bridge on same machine as HS.

1.  Run `-p /opt/matrix/matrix-lmtp` and add new user for bridge `useradd --system --shell /usr/sbin/nologin matrix-lmtp`, included systemd service will drop privileges to it.
2.  Copy `matrix-lmtp.service` to `/etc/systemd/system`.
    Copy files `color_log.py db_tables.py example-config.yaml lmtp_bridge.py requirements.txt` to newly created directory and `cd` to it.
3.  Create directory `/opt/matrix/matrix-lmtp/data` and change its owner to `matrix-lmtp`. With default configuration bridge will store there logs and database.
4.  You may want to drop privileges now as we are going to use pip now, remember to adjust directory permissions.
    Run `python3.7 -m venv ./venv` to create python virtual environment, then activate it with `. venv/bin/activate`.
    Run `pip install -r requirements.txt` to install dependencies.
5.  Adjust configuration file; you need at least to change domains, you also may want to change used ports and email templates.
    Upload configuration file as `config.yaml` then run `ln -s config.yaml example-config.yaml && chown matrix-lmtp config.yaml`. This is workaround for bug in used libs.
    If you allow only users from your domain to use bridge, you may remove `_{hs_domain}` from `*mail_template` options.
    Remember that config file contain appservice keys, so it must not be world readable!
6.  Generate registration file; Run `sudo -u matrix-lmtp ./venv/bin/python ./lmtp_bridge.py --generate-registration -r ./data/matrix-lmtp.yaml && chmod o-r ./data/matrix-lmtp.yaml`
    Copy registration file to synapse configuration dir. You also need to add this file to synapse config (section `app_service_config_files:`).
    Remember to adjust permissions so synapse can read this file.
7.  Last step is alembic initialization. Run `sudo -u matrix-lmtp bash` and `cd /opt/matrix/matrix-lmtp/data`.
    Run `alembic init alembic` and follow instructions. (Probably the only thing that you want to do is setting `sqlalchemy.url = sqlite:///lmtp-bridge.db` in `alembic.ini`)
    Finally, run `alembic upgrade head`.
8.  Run `systemctl enable matrix-lmtp` and restart synapse, it should also start bridge.
    Now pray and run `watch 'systemctl status -l matrix-synapse ; systemctl status -l matrix-lmtp'`, monitor it for ~20 seconds; synapse can start and die after few seconds,
    if registration file is malformed... BUT Installation should be done now :)

## Big TODOs
Due to bridge access control implemented in used libs, even when bridge is used to room only messages from whitelisted users will be sent (assuming that you limit bridge usage to your domain).
There is no support for message subject, cc and possibly other things.
It's not possible to unbridge room/remove user.
It's not possible to invite puppet to room, so new email users can not join invite only rooms in any way.
Bridge access control appears to be broken, it's possible to add only one token in `bridge.permissions`.
Configuration file can be overwritten by default values from `example-config.yaml`.
It's possible to enumerate users as bridge will return `554 no valid recipients` if new email would have any matrix recipient.
Bridge is not hardened against mail loops, some research needs to be done here.
#### LMPT is not fully implemented and under some rare conditions bridge may **silently** fail to deliver message.
#### If user leaves dynamically created room, it's not possible to go back.

## Things that does not work well, but would require lot of work
This bridge sends one mail on one matrix event, it means that when matrix user send file empty email with attachment will be sent.